<!DOCTYPE html> 
<html lang="{{ app()->getLocale() }}"> 
 
<head> 
  <meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta name="description" content=""> 
  <meta name="author" content=""> 
 
  <title>Sistema de Contabilidad ES - AccountingSys</title> 
 
  <!-- Fonts --> 
  <link href="lib/vendor/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css'> 
  <!-- Styles --> 
  <link href="lib/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 
  <link href="lib/vendor/bootstrap-toastr/toastr.min.css" rel="stylesheet"> 
  <link href="lib/vendor/jquery-ui/jquery-ui.min.css" rel="stylesheet"> 
  <link href="lib/vendor/primeui/primeui.min.css" rel="stylesheet"> 
  <link href="lib/vendor/primeui/themes/bootstrap/theme.css" rel="stylesheet"> 
  <!-- Custom style --> 
  <link href="lib/css/dashboard.css" rel="stylesheet"> 
  <!--  CSS Section --> 
  @stack('css') 
</head> 
 
<body> 
  <div id="wrapper"> 
 
    <!-- Navigation --> 
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0"> 
      <div class="navbar-header"> 
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
          <span class="sr-only">Toggle navigation</span> 
          <span class="icon-bar"></span> 
          <span class="icon-bar"></span> 
          <span class="icon-bar"></span> 
        </button> 
        <a class="navbar-brand" href="#">AccountingSys</a> 
      </div> 
      <!-- /.navbar-header --> 
 
      <ul class="nav navbar-top-links navbar-right"> 
        <li class="dropdown"> 
          <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
            <i class="fa fa-user fa-fw"></i> 
            <i class="fa fa-caret-down"></i> 
          </a> 
          <ul class="dropdown-menu dropdown-user"> 
            <li> 
              <a href="#"> 
                <i class="fa fa-unlock-alt fa-fw"></i> Cambiar contrase&ntilde;a</a> 
            </li> 
            <li> 
              <a href="#"> 
                <i class="fa fa-gear fa-fw"></i> Configuraci&oacute;n</a> 
            </li> 
            <li class="divider"></li> 
            <li> 
              <a href="#"> 
                <i class="fa fa-sign-out fa-fw"></i> Salir</a> 
            </li> 
          </ul> 
          <!-- /.dropdown-user --> 
        </li> 
        <!-- /.dropdown --> 
      </ul> 
      <!-- /.navbar-top-links --> 
      <div class="navbar-default sidebar" role="navigation"> 
       
      </div> 
      <!-- /.navbar-static-side --> 
    </nav> 
 
    <div id="page-wrapper"> 
      @yield('content') 
    </div> 
    <!-- /#page-wrapper --> 
    <footer class="footer"> 
      <div class="container"> 
        <i class="fa fa-registered" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; 
        <span class="text-muted">Sistemas y Avances Tecnol&oacute;gicos S.A de C.V</span> 
      </div> 
    </footer> 
  </div> 
  <!-- /#wrapper --> 
 
  <!-- JavaScripts --> 
  <script src="lib/vendor/jquery/jquery.min.js" type="text/javascript"></script> 
  <script src="lib/vendor/jquery-ui/jquery-ui.min.js" type="text/javascript"></script> 
  <script src="lib/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
  <script src="lib/vendor/metisMenu/metisMenu.min.js" type="text/javascript"></script> 
  <script src="lib/vendor/bootstrap-toastr/toastr.min.js" type="text/javascript"></script> 
  <script src="lib/vendor/bootstrap-validator/validator.min.js" type="text/javascript"></script> 
  <script src="lib/vendor/primeui/primeui.min.js" type="text/javascript"></script> 
 
  <!-- Custom Theme JavaScript --> 
  <script src="lib/js/dashboard.js"></script> 
 
  <!--  JavaScript Section --> 
  @stack('scripts') 
</body> 
 
</html>