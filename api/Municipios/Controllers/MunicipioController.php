<?php

namespace App\Http\Controllers\Catalogos;

use App\Model\Catalogos\Municipio;
use App\Model\Catalogos\Departamento;
use App\Http\Resources\Catalogos\MunicipioResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{
    /**
     * Display the main screen of the resource.
     *
     */
    public function myMunis()
    {
        return view('catalogo.municipios');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = array();
        foreach (Municipio::with('deptos')->get() as $data) {
            $result[] = ['id_muni' => $data->id_muni, 'nombre_muni' => $data->nombre_muni,'id_depto' => $data->deptos->id_depto,'nombre_depto' => $data->deptos->nombre_depto];
        }
        return datatables($result)->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  Primary key of the resource $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return MunicipioResource::collection($this->findById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Primary key of the resource $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre_muni' => 'bail|required',
        ]);
        $muni = Municipio::find($id);
        if ($muni) {
            $muni->nombre_muni =  $request->input('nombre_muni');
            $depto = Departamento::find($request->input('id_depto'));
            $muni->deptos()->associate($depto);
            $muni->update();
        }
    }

    /*
    *   Get tha data resource by Id
    */
    private function findById($id)
    {
        return Municipio::where('id_muni', $id)->get();
    }
}
