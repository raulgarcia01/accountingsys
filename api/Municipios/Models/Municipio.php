<?php

namespace App\Model\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    /**
     * Asociacion de modelo a tabla Municipios.
     *
     * @var string
     */
    protected $table = 'cat_municipios';

    /**
     * Nombre de la llave primaria
     *
     */
    protected   $primaryKey = 'id_muni';
    
    /**
     * Constantes de fechas de actualizacion
     *
     */
    const CREATED_AT = 'fecha_reg';
    const UPDATED_AT = 'fecha_mod';

    /**
     * Obtener Departamento asociado al Municipio.
     */
    public function deptos()
    {
        return $this->belongsTo('App\Model\Catalogos\Departamento', 'depto_id');
    }
}
