<?php

namespace Api\Roles\Model\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    /**
     * Asociacion de modelo a tabla Departamento.
     *
     * @var string
     */
    protected $table = 'adm_roles';

    /**
     * Nombre de la llave primaria
     *
     */
    protected $primaryKey = 'id_rol';

    protected $fillable = ['nombre_rol', 'estado', 'fecha_reg', 'fecha_mod'];


    /**
     * Constantes de fechas de actualizacion
     *
     */
    const CREATED_AT = 'fecha_reg';
    const UPDATED_AT = 'fecha_mod';
}
