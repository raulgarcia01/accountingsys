<?php

namespace App\Model\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    /**
     * Asociacion de modelo a tabla Departamento.
     *
     * @var string
     */
    protected $table = 'cat_departamentos';

    /**
     * Nombre de la llave primaria
     *
     */
    protected   $primaryKey = 'id_depto';

    /**
     * Constantes de fechas de actualizacion
     *
     */
    const CREATED_AT = 'fecha_reg';
    const UPDATED_AT = 'fecha_mod';

    /**
     * Obtener Municipios asociados al Departamento.
     */
    public function municipios()
    {
        return $this->hasMany('App\Model\Catalogos\Municipio');
    }
}
