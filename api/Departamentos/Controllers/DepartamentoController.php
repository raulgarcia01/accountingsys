<?php

namespace App\Http\Controllers\Catalogos;

use App\Model\Catalogos\Departamento;
use App\Http\Resources\Catalogos\DepartamentoResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DepartamentoController extends Controller
{

    //-- Para llamar un metodo dentro del controller es DepartamentoController::nombre_metodo
    /**
     * Display the main screen of the resource.
     *
     */
    public function myDeptos()
    {
        return view('catalogo.departamentos');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return datatables(Departamento::orderBy('id_depto', 'asc'))->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  Primary key of the resource $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DepartamentoResource::collection($this->findById($id));
    }

    /**
     * Display the specified resource.
     *
     * @param  Primary key of the resource $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $data = $request['q'];
        if ($data <> '') {
            return DB::table('cat_departamentos')->where('nombre_depto', 'like', $data.'%')->get();
        }
        return array();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Primary key of the resource $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre_depto' => 'bail|required',
        ]);
        $depto = Departamento::find($id);
        if ($depto) {
            $depto->nombre_depto =  $request->input('nombre_depto');
            $depto->update();
        }
    }

    /*
    *   Get tha data resource by Id
    */
    private function findById($id)
    {
        return Departamento::where('id_depto', $id)->get();
    }
}
