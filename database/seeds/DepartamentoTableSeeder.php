<?php

use Illuminate\Database\Seeder;

class DepartamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //-- Inserta informacion de catalogo a tabla Departamento
        DB::table('cat_municipios')->delete();
        DB::table('cat_departamentos')->delete();
        DB::table('cat_departamentos')->insert([['id_depto' =>'1', 'nombre_depto' =>'SAN SALVADOR', 'fecha_reg' => date('Y-m-d H:i:s')], 
                                                ['id_depto' =>'2', 'nombre_depto' =>'CUSCATLAN','fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'3', 'nombre_depto' =>'AHUACHAPAN','fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'4', 'nombre_depto' =>'MORAZAN', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'5', 'nombre_depto' =>'SAN VICENTE', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'6', 'nombre_depto' =>'CABAÑAS', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'7', 'nombre_depto' =>'SANTA ANA', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'8', 'nombre_depto' =>'SAN MIGUEL', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'9', 'nombre_depto' =>'LA LIBERTAD', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'10', 'nombre_depto' =>'USULUTAN', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'11', 'nombre_depto' =>'SONSONATE', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'12', 'nombre_depto' =>'LA UNION', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'13', 'nombre_depto' =>'LA PAZ', 'fecha_reg' => date('Y-m-d H:i:s')],
                                                ['id_depto' =>'14', 'nombre_depto' =>'CHALATENANGO', 'fecha_reg' => date('Y-m-d H:i:s')]]);
    }
}
