<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConSaldosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_saldos', function (Blueprint $table) {
            $table->increments('id_saldo');
            $table->integer('cta_id')->unsigned()->nullable($value = false);
            $table->integer('periodo_id')->unsigned()->nullable($value = false);
            $table->double('saldo_ini')->nullable($value = false);
            $table->double('cargos')->nullable($value = false);
            $table->double('abonos')->nullable($value = false);
            $table->double('saldo_final')->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('cta_id')->references('id_cta')->on('con_catalogo_cta');
            $table->foreign('periodo_id')->references('id_periodo')->on('con_periodos');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_saldos');
    }
}
