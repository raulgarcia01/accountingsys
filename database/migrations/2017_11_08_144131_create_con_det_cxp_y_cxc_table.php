<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConDetCxpYCxcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_det_cxp_y_cxc', function (Blueprint $table) {
            $table->increments('id_dcxpc');
            $table->integer('cxpc_id')->unsigned()->nullable($value = false);
            $table->double('mto_abono')->nullable($value = false);
            $table->double('mto_mora')->nullable($value = true);
            $table->timestamp('fecha_abono')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_vto')->nullable($value = true);
            $table->foreign('cxpc_id')->references('id_cxpc')->on('con_cxp_y_cxc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_det_cxp_y_cxc');
    }
}
