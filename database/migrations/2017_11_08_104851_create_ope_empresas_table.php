<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ope_empresas', function (Blueprint $table) {
            $table->increments('id_empresa');
            $table->string('razon_social', 250)->nullable($value = false);
            $table->integer('dias_credito')->nullable($value = true);
            $table->smallInteger('tipo')->nullable($value = true);         
            $table->smallInteger('fpago')->nullable($value = true);
            $table->smallInteger('pretencion')->nullable($value = true);
            $table->double('limite_credito')->nullable($value = true);
            $table->double('saldo_actual')->nullable($value = true);
            $table->smallInteger('tipo_empresa')->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ope_empresas');
    }
}
