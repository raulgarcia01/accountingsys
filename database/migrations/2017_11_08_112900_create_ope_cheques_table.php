<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeChequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ope_cheques', function (Blueprint $table) {
            $table->increments('id_cheques');
            $table->integer('num_cheq')->nullable($value = false);
            $table->timestamp('fecha_cheq')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->double('valor_cheq')->nullable($value = false);
            $table->string('nomb_repr', 250)->nullable($value = false);
            $table->longText('concepto', 250)->nullable($value = false);
            $table->smallInteger('estado')->nullable($value = false);
            $table->integer('empresa_id')->unsigned()->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('empresa_id')->references('id_empresa')->on('ope_empresas');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ope_cheques');
    }
}
