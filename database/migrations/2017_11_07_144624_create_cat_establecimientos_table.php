<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatEstablecimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_establecimientos', function (Blueprint $table) {
            $table->increments('id_establ');
            $table->string('nombre_establ', 250)->nullable($value = false);
            $table->string('giro', 250)->nullable($value = false);
            $table->string('crc', 10)->nullable($value = false);
            $table->string('nit', 14)->nullable($value = false);
            $table->string('direccion', 250)->nullable($value = false);
            $table->integer('establ_id')->unsigned();
            $table->integer('muni_id')->unsigned()->nullable($value = false);
            $table->integer('tipo_establ')->nullable($value = false);
            $table->string('telefonos', 100)->nullable($value = false);
            $table->string('correos', 100)->nullable($value = true);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('muni_id')->references('id_muni')->on('cat_municipios');
            $table->unique(['crc','id_establ']);
            $table->unique(['nit','id_establ']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_establecimientos');
    }
}
