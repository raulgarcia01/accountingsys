<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConDetPartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_det_partidas', function (Blueprint $table) {
            $table->increments('id_dptda');
            $table->integer('cta_id')->unsigned()->nullable($value = false);
            $table->double('cargo')->nullable($value = false);
            $table->double('abono')->nullable($value = false);
            $table->longText('concepto')->nullable($value = false);
            $table->integer('ptda_id')->unsigned()->nullable($value = false);
            $table->integer('cc_id')->unsigned()->nullable($value = false);
            $table->foreign('cta_id')->references('id_cta')->on('con_catalogo_cta');
            $table->foreign('ptda_id')->references('id_ptda')->on('con_partidas');
            $table->foreign('cc_id')->references('id_cc')->on('con_centro_costo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_det_partidas');
    }
}
