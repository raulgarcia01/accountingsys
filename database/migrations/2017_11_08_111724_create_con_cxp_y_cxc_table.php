<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConCxpYCxcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_cxp_y_cxc', function (Blueprint $table) {
            $table->increments('id_cxpc');
            $table->string('descripcion', 250)->nullable($value = true);
            $table->double('mto_total')->nullable($value = true);
            $table->smallInteger('tipo_mov')->nullable($value = true);
            $table->integer('docu_id')->unsigned()->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('docu_id')->references('id_docu')->on('ope_documentos');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_cxp_y_cxc');
    }
}
