<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_personas', function (Blueprint $table) {
            $table->increments('id_persona');
            $table->string('nombre1', 100)->nullable($value = false);
            $table->string('nombre2', 100)->nullable($value = true);
            $table->string('apellido1', 100)->nullable($value = false);
            $table->string('apellido2', 100)->nullable($value = true);
            $table->string('dui', 10)->nullable($value = false);
            $table->string('nit', 14)->nullable($value = false);
            $table->string('domicilio', 250)->nullable($value = true);
            $table->string('telefonos', 100)->nullable($value = false);
            $table->string('correos', 100)->nullable($value = true);
            $table->integer('muni_id')->unsigned()->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('muni_id')->references('id_muni')->on('cat_municipios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_personas');
    }
}
