<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ope_documentos', function (Blueprint $table) {
            $table->increments('id_docu');
            $table->smallInteger('tipo_docu')->nullable($value = true);
            $table->string('serie', 50)->nullable($value = true);
            $table->string('num_docu', 50)->nullable($value = false);
            $table->timestamp('fecha_docu')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('empresa_id')->unsigned()->nullable($value = false);
            $table->double('mto_afecto')->nullable($value = false);
            $table->double('mto_exento')->nullable($value = true);
            $table->double('mto_iva')->nullable($value = true);
            $table->double('mto_total')->nullable($value = false);
            $table->integer('docu_ref')->unsigned()->nullable($value = true);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
            $table->foreign('empresa_id')->references('id_empresa')->on('ope_empresas');
            $table->foreign('docu_ref')->references('id_docu')->on('ope_documentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ope_documentos');
    }
}
