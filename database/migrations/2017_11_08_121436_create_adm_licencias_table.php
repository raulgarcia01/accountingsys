<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmLicenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_licencias', function (Blueprint $table) {
            $table->increments('id_licencia');
            $table->timestamp('fecha_vto')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->longText('concepto', 250)->nullable($value = false);
            $table->integer('secret')->unsigned()->nullable($value = false);
            $table->smallInteger('estado')->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_licencias');
    }
}
