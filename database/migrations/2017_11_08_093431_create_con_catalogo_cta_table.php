<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConCatalogoCtaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_catalogo_cta', function (Blueprint $table) {
            $table->increments('id_cta');
            $table->string('conta_cta', 30)->nullable($value = false);
            $table->string('nombre_cta', 250)->nullable($value = false);
            $table->integer('nivel_cta')->nullable($value = false);
            $table->integer('tipo_cta')->nullable($value = false);
            $table->integer('cta_mayor')->unsigned()->nullable($value = true);
            $table->smallInteger('transac')->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('cta_mayor')->references('id_cta')->on('con_catalogo_cta');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_catalogo_cta');
    }
}
