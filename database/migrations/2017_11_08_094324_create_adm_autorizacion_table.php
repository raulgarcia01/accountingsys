<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmAutorizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_autorizacion', function (Blueprint $table) {
            $table->increments('id_auth');
            $table->integer('rol_id')->unsigned()->nullable($value = false);
            $table->integer('menu_id')->unsigned()->nullable($value = false);
            $table->smallInteger('crear')->nullable($value = false);
            $table->smallInteger('modificar')->nullable($value = false);
            $table->smallInteger('eliminar')->nullable($value = false);
            $table->smallInteger('autorizar')->nullable($value = false);
            $table->smallInteger('cierre')->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('rol_id')->references('id_rol')->on('adm_roles');
            $table->foreign('menu_id')->references('id_menu')->on('adm_menu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_autorizacion');
    }
}
