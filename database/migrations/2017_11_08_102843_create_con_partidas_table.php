<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConPartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_partidas', function (Blueprint $table) {
            $table->increments('id_ptda');
            $table->string('correlativo', 45)->nullable($value = false);
            $table->string('nombre_ptda', 250)->nullable($value = false);
            $table->longText('concepto')->nullable($value = false);
            $table->timestamp('fecha_ptda')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->double('total_cargo')->nullable($value = false);
            $table->double('total_abono')->nullable($value = false);
            $table->smallInteger('origen')->nullable($value = false);
            $table->integer('periodo_id')->unsigned()->nullable($value = false);
            $table->integer('tptda_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('periodo_id')->references('id_periodo')->on('con_periodos');
            $table->foreign('tptda_id')->references('id_tptda')->on('cat_tipo_ptda');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_partidas');
    }
}
