<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConAsocEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('con_asoc_empresas', function (Blueprint $table) {
            $table->increments('id_asoc_con');
            $table->integer('cta_id')->unsigned()->nullable($value = false);
            $table->integer('empresa_id')->unsigned()->nullable($value = true);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->integer('cxpc_id')->unsigned()->nullable($value = true);
            $table->smallInteger('tipo_asoc')->nullable($value = true);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('cta_id')->references('id_cta')->on('con_catalogo_cta');
            $table->foreign('empresa_id')->references('id_empresa')->on('ope_empresas');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
            $table->foreign('cxpc_id')->references('id_cxpc')->on('con_cxp_y_cxc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('con_asoc_empresas');
    }
}
