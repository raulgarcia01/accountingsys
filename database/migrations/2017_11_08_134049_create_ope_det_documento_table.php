<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeDetDocumentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ope_det_documento', function (Blueprint $table) {
            $table->increments('id_ddocu');
            $table->integer('docu_id')->unsigned()->nullable($value = false);
            $table->integer('prod_id')->unsigned()->nullable($value = false);
            $table->integer('cantidad')->nullable($value = false);
            $table->longText('descripcion', 250)->nullable($value = false);
            $table->double('precio_unit')->nullable($value = true);
            $table->double('val_total')->nullable($value = false);
            $table->foreign('docu_id')->references('id_docu')->on('ope_documentos');
            $table->foreign('prod_id')->references('id_prod')->on('ope_productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ope_det_documento');
    }
}
