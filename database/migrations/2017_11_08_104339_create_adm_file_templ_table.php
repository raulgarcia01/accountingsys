<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmFileTemplTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_file_templ', function (Blueprint $table) {
            $table->increments('id_ftempl');
            $table->string('nombre_archivo', 100)->nullable($value = false);
            $table->binary('archivo')->nullable($value = false);
            $table->smallInteger('tipo')->nullable($value = false);         
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('usuario_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('usuario_id')->references('id_usuario')->on('adm_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_file_templ');
    }
}
