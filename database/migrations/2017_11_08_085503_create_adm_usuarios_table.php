<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_usuarios', function (Blueprint $table) {
            $table->increments('id_usuario');
            $table->string('usuario', 20)->nullable($value = false);
            $table->string('clave', 250)->nullable($value = false);
            $table->smallInteger('vigencia')->nullable($value = false);
            $table->smallInteger('estado')->nullable($value = false);
            $table->timestamp('fecha_cmbclave')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('persona_id')->unsigned()->nullable($value = false);
            $table->integer('establ_id')->unsigned()->nullable($value = false);
            $table->integer('rol_id')->unsigned()->nullable($value = false);
            $table->smallInteger('tipo_tc')->nullable($value = true);
            $table->string('tarjeta_credito', 50)->nullable($value = true);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('persona_id')->references('id_persona')->on('cat_personas');
            $table->foreign('establ_id')->references('id_establ')->on('cat_establecimientos');
            $table->foreign('rol_id')->references('id_rol')->on('adm_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_usuarios');
    }
}
