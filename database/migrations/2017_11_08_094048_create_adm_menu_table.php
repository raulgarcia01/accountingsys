<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adm_menu', function (Blueprint $table) {
            $table->increments('id_menu');
            $table->string('nombre_menu', 250)->nullable($value = false);
            $table->string('icono', 50)->nullable($value = false);
            $table->string('ruta_item', 250)->nullable($value = false);
            $table->smallInteger('modulo')->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adm_menu');
    }
}
