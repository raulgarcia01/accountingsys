<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_municipios', function (Blueprint $table) {
            $table->increments('id_muni');
            $table->string('nombre_muni', 100)->nullable($value = false);
            $table->integer('depto_id')->unsigned()->nullable($value = false);
            $table->timestamp('fecha_reg')->nullable($value = false)->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('fecha_mod')->nullable($value = true);
            $table->foreign('depto_id')->references('id_depto')->on('cat_departamentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_municipios');
    }
}
