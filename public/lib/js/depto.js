$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

//-- Set the Datatable Object
$(document).ready(function() {
    var table = $('#dataTable-depto').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "/depto",
        columns: [
            { data: 'id_depto', name: 'id_depto' },
            { data: 'nombre_depto', name: 'nombre_depto' },
        ],
        language: {
            "decimal":        ".",
            "emptyTable":     "No existen registros disponibles",
            "info":           "Mostrando _START_ de _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
            "infoFiltered":   "(Filtrado de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No existen registros coincidentes",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar en orden ascendente",
                "sortDescending": ": Activar en orden descendente"
            }
        }            
    });

    //-- Colocar de elemento seleccionado la datatable
    $('#dataTable-depto').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.rows( table.row(this).index() ).data();
        $('#row-selected').val(data[0]['id_depto']);
    });

    //-- Click al para mostrar data en el Editar
    $( "#showBtn" ).click(function() {
        var id_depto = $('#row-selected').val();
        $.get('/depto/' + id_depto, function (data) {
            if(data['data'].length > 0){
                $('#deptoId').val(data['data'][0].id_depto);
                $('#deptoNombre').val(data['data'][0].nombre_depto);
                $('#deptoModal').modal('show');
            }else{
                setMessage('MESS_INFO', 'Importante', 'Debe seleccionar un elemento de la lista');
            }
        })
    });

    //-- Click al para mostrar data en el Editar
    $( "#updateBtn" ).click(function() {
        $('#deptoForm').submit();
    });

    //-- Modificar registro
    $('#deptoModal').on('hidden.bs.modal', function () {
        $('#deptoForm')[0].reset();
    });

    //--Validador del formulario
    $('#deptoForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            setMessage('MESS_ERRO', 'Importante', 'Debe completar los campos requeridos');
        } else {
            e.preventDefault(); 

            $.ajax({
                url: '/depto/' + $('#deptoId').val(),
                type: 'PUT',
                data: { 
                    'nombre_depto': $('#deptoNombre').val()
                },
                success: function(data) {
                    $('#dataTable-depto').DataTable().ajax.reload(function ( json ) {
                        setMessage('MESS_SUCC', 'Aviso', 'Informaci&oacute;n actualizada correctamente');
                    },false);
                    $('#dataTable-depto tbody tr').removeClass('selected');
                    $('#row-selected').val(0);
                    $('#deptoModal').modal('hide');
                }
            });
        }
    });
});