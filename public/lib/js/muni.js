$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

var languageVar =  {
    "decimal":        ".",
    "emptyTable":     "No existen registros disponibles",
    "info":           "Mostrando _START_ de _END_ de _TOTAL_ registros",
    "infoEmpty":      "Mostrando 0 de 0 de 0 registros",
    "infoFiltered":   "(Filtrado de _MAX_ registros)",
    "infoPostFix":    "",
    "thousands":      ",",
    "lengthMenu":     "Mostrar _MENU_ registros",
    "loadingRecords": "Cargando...",
    "processing":     "Procesando...",
    "search":         "Buscar:",
    "zeroRecords":    "No existen registros coincidentes",
    "paginate": {
        "first":      "Primero",
        "last":       "Ultimo",
        "next":       "Siguiente",
        "previous":   "Anterior"
    },
    "aria": {
        "sortAscending":  ": Activar en orden ascendente",
        "sortDescending": ": Activar en orden descendente"
    }
  }

//-- Set the Datatable Object
$(document).ready(function() {
    var table = $('#dataTable-muni').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: "/muni",
        columns: [
            { data: 'id_muni', name: 'id_muni' },
            { data: 'nombre_muni', name: 'nombre_muni' },
            { data: 'nombre_depto', name: 'nombre_depto' },
        ],
        language: languageVar
    });

    //-- Colocar de elemento seleccionado la datatable
    $('#dataTable-muni').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var data = table.rows( table.row(this).index() ).data();
        $('#row-selected').val(data[0]['id_muni']);
    });

    //-- Click al para mostrar data en el Editar
    $( "#showBtn" ).click(function() {
        var id_muni = $('#row-selected').val();
        $.get('/muni/' + id_muni, function (data) {
            if(data['data'].length > 0){
                $('#muniId').val(data['data'][0].id_muni);
                $('#muniNombre').val(data['data'][0].nombre_muni);
                $('#muniDepto').val(data['data'][0].id_depto).selectpicker('refresh');
                $('#muniModal').modal('show');
            }else{
                setMessage('MESS_INFO', 'Importante', 'Debe seleccionar un elemento de la lista');
            }
        })
    });

    //-- Click al para mostrar data en el Editar
    $( "#updateBtn" ).click(function() {
        $('#muniForm').submit();
    });

    //-- Modificar registro
    $('#muniModal').on('hidden.bs.modal', function () {
        $('#muniForm')[0].reset();
    });

    //--Validador del formulario
    $('#muniForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            setMessage('MESS_ERRO', 'Importante', 'Debe completar los campos requeridos');
        } else {
            e.preventDefault();

            $.ajax({
                url: '/muni/' + $('#muniId').val(),
                type: 'PUT',
                data: {
                    'nombre_muni': $('#muniNombre').val(),
                    'id_depto': $('#muniDepto').val()
                },
                success: function(data) {
                    $('#dataTable-muni').DataTable().ajax.reload(function ( json ) {
                        setMessage('MESS_SUCC', 'Aviso', 'Informaci&oacute;n actualizada correctamente');
                    },false);
                    $('#dataTable-muni tbody tr').removeClass('selected');
                    $('#row-selected').val(0);
                    $('#muniModal').modal('hide');
                }
            });
        }
    });

    $.get('/depto/', function (data) {
        if(data['data'].length > 0){
            var options;
            for (var i = 0; i < data['data'].length; i++) {
                options += "<option value='" + data['data'][i]['id_depto'] + "'>" + data['data'][i]['nombre_depto'] + "</option>";
            }
            $("#muniDepto").append(options);
            $('#muniDepto').selectpicker('refresh');
        }
    });

});
