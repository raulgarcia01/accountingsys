<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartamentoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetDepartamento()
    {
        $this->assertDatabaseHas('cat_departamento', [
            'nombre_depto' => 'San Salvador'
        ]);
    }
}
