<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('/deptos', 'Catalogos\DepartamentoController@myDeptos');

Route::resource(
    'depto',
    'Catalogos\DepartamentoController',
                ['only' => ['index','show','update']]
);

Route::post('depto/search/', ['as' => 'depto.search','uses' => 'Catalogos\DepartamentoController@search']);

Route::get('/munis', 'Catalogos\MunicipioController@myMunis');

Route::resource(
    'muni',
    'Catalogos\MunicipioController',
                ['only' => ['index','show','update']]
);
